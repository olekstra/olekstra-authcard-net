﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Olekstra.Authcard.Objects;

namespace Olekstra.Authcard
{
    public class AuthcardService : IAuthcardService
    {
        private readonly AuthcardServiceOptions options;

        public AuthcardService(AuthcardServiceOptions options)
        {
            this.options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public Task<ConfirmResponse> ConfirmAsync(ConfirmRequestType1 confirmRequest)
        {
            string path = "/confirm.xml";
            return ExecuteRequestAsync<ConfirmResponse>(confirmRequest, path);
        }

        public Task<VoucherResponse> CheckAsync(VoucherRequestType1 voucherRequest)
        {
            string path = "/check.xml";
            return ExecuteRequestAsync<VoucherResponse>(voucherRequest, path);
        }

        public Task<ConfirmResponse> ConfirmExAsync(ConfirmRequestType2 confirmRequest)
        {
            string path = "/confirm.ext.xml";
            return ExecuteRequestAsync<ConfirmResponse>(confirmRequest, path);
        }

        public Task<VoucherResponse> CheckExAsync(VoucherRequestType2 voucherRequest)
        {
            string path = "/check.ext.xml";
            return ExecuteRequestAsync<VoucherResponse>(voucherRequest, path);
        }

        public Task<InvalidateResponse> InvalidateAsync(InvalidateRequest invalidateRequest)
        {
            string path = "/invalidate";
            return ExecuteRequestAsync<InvalidateResponse>(invalidateRequest, path);
        }

        public string SerializeJson(object value)
        {
            return JsonConvert.SerializeObject(value, options.JsonSerializerOptions);
        }

        public T DeserializeJson<T>(string json)
        {
            return (T)JsonConvert.DeserializeObject(json, typeof(T), options.JsonSerializerOptions);
        }

        public string SerializeXml(object value)
        {
            var serializer = new XmlSerializer(value.GetType());

            using (var ms = new MemoryStream())
            {
                using (var xw = XmlWriter.Create(ms, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = false, Encoding = Encoding.ASCII }))
                {
                    var xmlns = new XmlSerializerNamespaces();
                    xmlns.Add(string.Empty, string.Empty);

                    serializer.Serialize(xw, value, xmlns);

                    return Encoding.ASCII.GetString(ms.ToArray());
                }
            }
        }

        public void SerializeXml(object value, Stream stream)
        {
            var serializer = new XmlSerializer(value.GetType());

            using (var xw = XmlWriter.Create(stream, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = false, Encoding = Encoding.ASCII, CloseOutput = false }))
            {
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add(string.Empty, string.Empty);

                serializer.Serialize(xw, value, xmlns);
            }
        }

        public T DeserializeXml<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var sr = new StringReader(xml))
            {
                using (var xmlReader = XmlReader.Create(sr, options.XmlReaderSettings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }

        public T DeserializeXml<T>(Stream stream)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var xmlReader = XmlReader.Create(stream, options.XmlReaderSettings))
            {
                return (T)serializer.Deserialize(xmlReader);
            }
        }

        private async Task<TResponse> ExecuteRequestAsync<TResponse>(object data, string path)
            where TResponse : new()
        {
            var request = WebRequest.Create(options.BaseUrl + path);
            request.Method = "POST";
            request.ContentType = "application/xml";

            var requestStream = await request.GetRequestStreamAsync();
            SerializeXml(data, requestStream);

            var response = await request.GetResponseAsync();
            using (var ms = new MemoryStream())
            {
                using (var rs = response.GetResponseStream())
                {
                    return DeserializeXml<TResponse>(rs);
                }
            }
        }
    }
}
