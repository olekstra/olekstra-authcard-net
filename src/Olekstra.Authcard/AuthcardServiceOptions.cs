﻿namespace Olekstra.Authcard
{
    using System;
    using System.Globalization;
    using System.Xml;
    using System.Xml.Schema;
    using Newtonsoft.Json;

    public class AuthcardServiceOptions
    {
        public string BaseUrl { get; set; } = "http://authcard.olekstra.ru";

        /// <summary>
        /// Настройки JSON-сериализации.
        /// </summary>
        public JsonSerializerSettings JsonSerializerOptions { get; set; } = CreateDefaultJsonSerializerOptions();

        public XmlReaderSettings XmlReaderSettings { get; set; } = CreateXmlReaderSettings();

        /// <summary>
        /// Создает настройки по умолчанию для JSON-сериализатора.
        /// </summary>
        /// <remarks>
        /// В объект вносятся следующие изменения:
        /// * <see cref="JsonSerializerSettings.Culture"/> устанавливается в CultureInfo.InvariantCulture
        /// </remarks>
        /// <returns>Настройки.</returns>
        public static JsonSerializerSettings CreateDefaultJsonSerializerOptions()
        {
            return new JsonSerializerSettings
            {
                Culture = CultureInfo.InvariantCulture,
            };
        }

        public static XmlReaderSettings CreateXmlReaderSettings()
        {
            return new XmlReaderSettings() {
                Schemas = LoadSchemas(),
                ValidationType = ValidationType.Schema,
            };
        }

        private static XmlSchemaSet LoadSchemas()
        {
            var assembly = typeof(AuthcardServiceOptions).Assembly;

            var schemas = new XmlSchemaSet();
            using (var sr = assembly.GetManifestResourceStream("Olekstra.Authcard.authcard.xsd"))
            {
                schemas.Add(null, XmlReader.Create(sr));
            }
            return schemas;
        }
    }
}
