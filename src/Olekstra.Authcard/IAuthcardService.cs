﻿using System.Threading.Tasks;
using Olekstra.Authcard.Objects;

namespace Olekstra.Authcard
{
    public interface IAuthcardService
    {
        Task<VoucherResponse> CheckAsync(VoucherRequestType1 voucherRequest);
        Task<VoucherResponse> CheckExAsync(VoucherRequestType2 voucherRequest);
        Task<ConfirmResponse> ConfirmAsync(ConfirmRequestType1 confirmRequest);
        Task<ConfirmResponse> ConfirmExAsync(ConfirmRequestType2 confirmRequest);
        Task<InvalidateResponse> InvalidateAsync(InvalidateRequest invalidateRequest);
    }
}