﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "invalidateRequest")]
    [XmlRootAttribute(Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "invalidateRequest")]
    public partial class InvalidateRequest
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("drugstoreId")]
        [XmlElementAttribute(Order = 0, ElementName = "drugstoreId")]
        public string DrugstoreId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("voucherId")]
        [XmlElementAttribute(Order = 1, ElementName = "voucherId")]
        public string VoucherId { get; set; }

        [JsonProperty("voucherDate")]
        [XmlElementAttribute(DataType = "date", Order = 2, ElementName = "voucherDate")]
        public System.DateTime VoucherDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("authorizationCode")]
        [XmlElementAttribute(Order = 3, ElementName = "authorizationCode")]
        public string AuthorizationCode { get; set; }

        [JsonProperty("reason")]
        [XmlElementAttribute(Order = 4, ElementName = "reason")]
        public string Reason { get; set; }
    }
}
