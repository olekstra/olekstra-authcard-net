﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.Collections;
    using System.Xml.Schema;
    using System.ComponentModel;
    using System.Xml;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "voucherRequestType1Product")]
    [XmlRootAttribute("voucherRequestType1Product")]
    public partial class VoucherRequestType1Product
    {
        [JsonProperty("clientPackId")]
        [XmlElementAttribute(Order = 0, ElementName = "clientPackId")]
        public string ClientPackId { get; set; }

        [JsonProperty("clientPackName")]
        [XmlElementAttribute(Order = 1, ElementName = "clientPackName")]
        public string ClientPackName { get; set; }

        [JsonProperty("ekt")]
        [XmlElementAttribute(Order = 2, ElementName = "ekt")]
        public string Ekt { get; set; }

        [JsonProperty("barcode")]
        [XmlElementAttribute(Order = 3, ElementName = "barcode")]
        public string Barcode { get; set; }

        [JsonProperty("quantity")]
        [XmlElementAttribute(DataType = "positiveInteger", Order = 4, ElementName = "quantity")]
        public string Quantity { get; set; }

        [JsonProperty("price")]
        [XmlElementAttribute(Order = 5, ElementName = "price")]
        public decimal Price { get; set; }

        [JsonProperty("decNumber")]
        [XmlElementAttribute(Order = 6, ElementName = "decNumber")]
        public string DecNumber { get; set; }

        [JsonProperty("expireYear")]
        [XmlElementAttribute(Order = 7, ElementName = "expireYear")]
        public int ExpireYear { get; set; }

        [XmlIgnore]
        public bool ExpireYearSpecified { get; set; }

        [JsonProperty("exactSumToPay")]
        [XmlElementAttribute(Order = 8, ElementName = "exactSumToPay")]
        public decimal ExactSumToPay { get; set; }

        [JsonProperty("exactRefundSum")]
        [XmlElementAttribute(Order = 9, ElementName = "exactRefundSum")]
        public decimal ExactRefundSum { get; set; }

        [JsonProperty("exactNonRefundSum")]
        [XmlElementAttribute(Order = 10, ElementName = "exactNonRefundSum")]
        public decimal ExactNonRefundSum { get; set; }
    }
}
