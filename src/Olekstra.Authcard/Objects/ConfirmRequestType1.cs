﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    [XmlIncludeAttribute(typeof(ConfirmRequestType2))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "confirmRequestType1")]
    [XmlRootAttribute("confirmRequest", Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "confirmRequest")]
    public partial class ConfirmRequestType1
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("drugstoreId")]
        [XmlElementAttribute(Order = 0, ElementName = "drugstoreId")]
        public string DrugstoreId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("voucherId")]
        [XmlElementAttribute(Order = 1, ElementName = "voucherId")]
        public string VoucherId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("confirmationToken")]
        [XmlElementAttribute(Order = 2, ElementName = "confirmationToken")]
        public string ConfirmationToken { get; set; }
    }
}
