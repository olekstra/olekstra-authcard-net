﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "voucherResponse")]
    [XmlRootAttribute(Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "voucherResponse")]
    public partial class VoucherResponse
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("voucherId")]
        [XmlElementAttribute(Order = 0, ElementName = "voucherId")]
        public string VoucherId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("drugstoreId")]
        [XmlElementAttribute(Order = 1, ElementName = "drugstoreId")]
        public string DrugstoreId { get; set; }

        [JsonProperty("card")]
        [XmlElementAttribute(Order = 2, ElementName = "card")]
        public VoucherResponseCard Card { get; set; }

        [JsonProperty("sumWithoutDiscount")]
        [XmlElementAttribute(Order = 3, ElementName = "sumWithoutDiscount")]
        public decimal SumWithoutDiscount { get; set; }

        [JsonProperty("discountSum")]
        [XmlElementAttribute(Order = 4, ElementName = "discountSum")]
        public decimal DiscountSum { get; set; }

        [JsonProperty("sumToPay")]
        [XmlElementAttribute(Order = 5, ElementName = "sumToPay")]
        public decimal SumToPay { get; set; }

        [JsonProperty("warnings")]
        [XmlElementAttribute("warning", Order = 6, ElementName = "warning")]
        public List<string> Warning { get; set; }

        [JsonProperty("rows")]
        [XmlElementAttribute("row", Order = 7, ElementName = "row")]
        public List<VoucherResponseRow> Row { get; set; }

        [JsonProperty("confirmationToken")]
        [XmlElementAttribute(Order = 8, ElementName = "confirmationToken")]
        public string ConfirmationToken { get; set; }

        [JsonProperty("exactSumToPay")]
        [XmlElementAttribute(Order = 9, ElementName = "exactSumToPay")]
        public decimal ExactSumToPay { get; set; }

        [JsonProperty("exactRefundSum")]
        [XmlElementAttribute(Order = 10, ElementName = "exactRefundSum")]
        public decimal ExactRefundSum { get; set; }

        [JsonProperty("exactNonRefundSum")]
        [XmlElementAttribute(Order = 11, ElementName = "exactNonRefundSum")]
        public decimal ExactNonRefundSum { get; set; }

        [JsonProperty("complete")]
        [XmlElementAttribute(Order = 12, ElementName = "complete")]
        public bool Complete { get; set; }

        [JsonProperty("authorizationCode")]
        [XmlElementAttribute(Order = 13, ElementName = "authorizationCode")]
        public string AuthorizationCode { get; set; }

        public VoucherResponse()
        {
            Row = new List<VoucherResponseRow>();
            Card = new VoucherResponseCard();
        }
    }
}
