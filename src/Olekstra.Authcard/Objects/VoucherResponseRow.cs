﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "voucherResponseRow")]
    [XmlRootAttribute("voucherResponseRow")]
    public partial class VoucherResponseRow
    {
        [JsonProperty("clientPackId")]
        [XmlElementAttribute(Order = 0, ElementName = "clientPackId")]
        public string ClientPackId { get; set; }

        [JsonProperty("clientPackName")]
        [XmlElementAttribute(Order = 1, ElementName = "clientPackName")]
        public string ClientPackName { get; set; }

        [JsonProperty("id")]
        [XmlElementAttribute(Order = 2, ElementName = "id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        [XmlElementAttribute(Order = 3, ElementName = "name")]
        public string Name { get; set; }

        [JsonProperty("quantity")]
        [XmlElementAttribute(DataType = "positiveInteger", Order = 4, ElementName = "quantity")]
        public string Quantity { get; set; }

        [JsonProperty("price")]
        [XmlElementAttribute(Order = 5, ElementName = "price")]
        public decimal Price { get; set; }

        [JsonProperty("sumWithoutDiscount")]
        [XmlElementAttribute(Order = 6, ElementName = "sumWithoutDiscount")]
        public decimal SumWithoutDiscount { get; set; }

        [JsonProperty("discountPercent")]
        [XmlElementAttribute(Order = 7, ElementName = "discountPercent")]
        public int DiscountPercent { get; set; }

        [JsonProperty("discountSum")]
        [XmlElementAttribute(Order = 8, ElementName = "discountSum")]
        public decimal DiscountSum { get; set; }

        [JsonProperty("sumToPay")]
        [XmlElementAttribute(Order = 9, ElementName = "sumToPay")]
        public decimal SumToPay { get; set; }

        [JsonProperty("refundSum")]
        [XmlElementAttribute(Order = 10, ElementName = "refundSum")]
        public decimal RefundSum { get; set; }

        [JsonProperty("nonRefundPercent")]
        [XmlElementAttribute(Order = 11, ElementName = "nonRefundPercent")]
        public int NonRefundPercent { get; set; }

        [JsonProperty("replacePack")]
        [XmlElementAttribute("replacePack", Order = 12, ElementName = "replacePack")]
        public List<VoucherResponseRowReplacePack> ReplacePack { get; set; }

        [JsonProperty("warnings")]
        [XmlElementAttribute("warning", Order = 13, ElementName = "warning")]
        public List<string> Warning { get; set; }

        [JsonProperty("exactSumToPay")]
        [XmlElementAttribute(Order = 14, ElementName = "exactSumToPay")]
        public decimal ExactSumToPay { get; set; }

        [JsonProperty("exactRefundSum")]
        [XmlElementAttribute(Order = 15, ElementName = "exactRefundSum")]
        public decimal ExactRefundSum { get; set; }

        [JsonProperty("exactNonRefundSum")]
        [XmlElementAttribute(Order = 16, ElementName = "exactNonRefundSum")]
        public decimal ExactNonRefundSum { get; set; }

        public VoucherResponseRow()
        {
            ReplacePack = new List<VoucherResponseRowReplacePack>();
        }
    }
}
