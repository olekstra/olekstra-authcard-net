﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.Xml;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Olekstra.Authcard.Objects
{
    [XmlIncludeAttribute(typeof(VoucherRequestType2))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "voucherRequestType1")]
    [XmlRootAttribute("voucherRequest", Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "voucherRequest")]
    public partial class VoucherRequestType1
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("drugstoreId")]
        [XmlElementAttribute(Order = 0, ElementName = "drugstoreId")]
        public string DrugstoreId { get; set; }

        [JsonProperty("voucherId")]
        [XmlElementAttribute(Order = 1, ElementName = "voucherId")]
        public string VoucherId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("cardBarcode")]
        [XmlElementAttribute(Order = 2, ElementName = "cardBarcode")]
        public string CardBarcode { get; set; }

        [JsonProperty("products")]
        [XmlElementAttribute("product", Order = 3, ElementName = "product")]
        public List<VoucherRequestType1Product> Product { get; set; }

        [JsonProperty("sumToPay")]
        [XmlElementAttribute(Order = 4, ElementName = "sumToPay")]
        public decimal SumToPay { get; set; }

        [JsonProperty("exactSumToPay")]
        [XmlElementAttribute(Order = 5, ElementName = "exactSumToPay")]
        public decimal ExactSumToPay { get; set; }

        [JsonProperty("exactRefundSum")]
        [XmlElementAttribute(Order = 6, ElementName = "exactRefundSum")]
        public decimal ExactRefundSum { get; set; }

        [JsonProperty("exactNonRefundSum")]
        [XmlElementAttribute(Order = 7, ElementName = "exactNonRefundSum")]
        public decimal ExactNonRefundSum { get; set; }

        public VoucherRequestType1()
        {
            Product = new List<VoucherRequestType1Product>();
        }
    }
}
