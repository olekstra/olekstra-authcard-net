﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "confirmRequestType2")]
    [XmlRootAttribute("confirmRequest2", Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "confirmRequest2")]
    public partial class ConfirmRequestType2
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("drugstoreId")]
        [XmlElementAttribute(Order = 0, ElementName = "drugstoreId")]
        public string DrugstoreId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("voucherId")]
        [XmlElementAttribute(Order = 1, ElementName = "voucherId")]
        public string VoucherId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("confirmationToken")]
        [XmlElementAttribute(Order = 2, ElementName = "confirmationToken")]
        public string ConfirmationToken { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("drugstoreSecret")]
        [XmlElementAttribute(Order = 3, ElementName = "drugstoreSecret")]
        public string DrugstoreSecret { get; set; }

        [JsonProperty("transactionStart")]
        [XmlElementAttribute(Order = 4, ElementName = "transactionStart")]
        public System.DateTime TransactionStart { get; set; }
    }
}
