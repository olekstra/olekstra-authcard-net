﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "invalidateResponse")]
    [XmlRootAttribute(Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "invalidateResponse")]
    public partial class InvalidateResponse
    {
        [JsonProperty("success")]
        [XmlAttributeAttribute(AttributeName = "success")]
        public bool Success { get; set; }

        public InvalidateResponse()
        {
            Success = true;
        }
    }
}
