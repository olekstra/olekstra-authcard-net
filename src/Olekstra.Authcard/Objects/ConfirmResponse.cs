﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "confirmResponse")]
    [XmlRootAttribute(Namespace = "http://www.olekstra.ru/schema/cards", IsNullable = false, ElementName = "confirmResponse")]
    public partial class ConfirmResponse
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("voucherId")]
        [XmlElementAttribute(Order = 0, ElementName = "voucherId")]
        public string VoucherId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.ValueRequired))]
        [JsonProperty("authorizationCode")]
        [XmlElementAttribute(Order = 1, ElementName = "authorizationCode")]
        public string AuthorizationCode { get; set; }
    }
}
