﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "voucherResponseRowReplacePack")]
    [XmlRootAttribute("voucherResponseRowReplacePack")]
    public partial class VoucherResponseRowReplacePack
    {
        [JsonProperty("packId")]
        [XmlElementAttribute(Order = 0, ElementName = "packId")]
        public string PackId { get; set; }

        [JsonProperty("barcode")]
        [XmlElementAttribute(Order = 1, ElementName = "barcode")]
        public string Barcode { get; set; }

        [JsonProperty("tradeName")]
        [XmlElementAttribute(Order = 2, ElementName = "tradeName")]
        public string TradeName { get; set; }
    }
}
