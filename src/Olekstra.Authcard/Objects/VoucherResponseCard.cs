﻿namespace Olekstra.Authcard.Objects
{
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.olekstra.ru/schema/cards", TypeName = "voucherResponseCard")]
    [XmlRootAttribute("voucherResponseCard")]
    public partial class VoucherResponseCard
    {
        [JsonProperty("number")]
        [XmlElementAttribute(Order = 0, ElementName = "number")]
        public string Number { get; set; }

        [JsonProperty("owner")]
        [XmlElementAttribute(Order = 1, ElementName = "owner")]
        public string Owner { get; set; }

        [JsonProperty("active")]
        [XmlElementAttribute(Order = 2, ElementName = "active")]
        public bool Active { get; set; }

        [JsonProperty("planName")]
        [XmlElementAttribute(Order = 3, ElementName = "planName")]
        public string PlanName { get; set; }

        [JsonProperty("comment")]
        [XmlElementAttribute(Order = 4, ElementName = "comment")]
        public string Comment { get; set; }

        [JsonProperty("checks")]
        [XmlElementAttribute("check", Order = 5, ElementName = "check")]
        public List<string> Check { get; set; }

        [JsonProperty("planId")]
        [XmlElementAttribute(Order = 6, ElementName = "planId")]
        public string PlanId { get; set; }
    }
}
