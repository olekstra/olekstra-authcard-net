﻿namespace Olekstra.Authcard
{
    using System;
    using Xunit;

    public abstract class SerializationTestsBase<T>
    {
        protected readonly AuthcardService authcardService;

        private readonly T sample;
        private readonly string xml;
        private readonly string json;

        public SerializationTestsBase(T sample, string xml, string json, bool doNotEscapePlusSign = false)
        {
            this.sample = sample ?? throw new ArgumentNullException(nameof(sample));
            this.xml = xml;
            this.json = json;
            this.authcardService = new AuthcardService(new AuthcardServiceOptions());
        }

        public abstract void ValidateObject(T value);

        [Fact]
        public void JsonSerializationOk()
        {
            ValidateObject(sample);

            var json1 = authcardService.SerializeJson(sample);

            var value = authcardService.DeserializeJson<T>(json1);
            Assert.NotNull(value);

            ValidateObject(value);
            var json2 = authcardService.SerializeJson(value);

            Assert.Equal(json1, json2);
        }

        [Fact]
        public void XmlSerializationOk()
        {
            ValidateObject(sample);

            var xml1 = authcardService.SerializeXml(sample);

            var value = authcardService.DeserializeXml<T>(xml1);
            Assert.NotNull(value);

            ValidateObject(value);
            var xml2 = authcardService.SerializeXml(value);

            Assert.Equal(xml1, xml2);
        }

        [Fact]
        public void XmlDeserlizationOk()
        {
            if (!string.IsNullOrEmpty(xml))
            {
                var value = authcardService.DeserializeXml<T>(xml);
                Assert.NotNull(value);
                ValidateObject(value);
            }
        }

        [Fact]
        public void JsonDeserlizationOk()
        {
            if (!string.IsNullOrEmpty(json))
            {
                var value = authcardService.DeserializeJson<T>(json);
                Assert.NotNull(value);
                ValidateObject(value);
            }
        }
    }
}
