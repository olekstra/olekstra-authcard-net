﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;
    using System;

    public class ConfirmRequestType2Test: SerializationTestsBase<ConfirmRequestType2>
    {
        private static readonly ConfirmRequestType2 sample = new ConfirmRequestType2()
        {
            ConfirmationToken = Guid.NewGuid().ToString(),
            DrugstoreId = "testdrugstoreid",
            VoucherId = Guid.NewGuid().ToString(),
            DrugstoreSecret = "testdrugstoresicret",
            TransactionStart = DateTime.Now,
        };

        public ConfirmRequestType2Test()
            : base(sample, null, null)
        {
            // Nothing
        }

        public override void ValidateObject(ConfirmRequestType2 value)
        {
            Assert.Equal(sample.ConfirmationToken, value.ConfirmationToken);
            Assert.Equal(sample.DrugstoreId, value.DrugstoreId);
            Assert.Equal(sample.VoucherId, value.VoucherId);
            Assert.Equal(sample.DrugstoreSecret, value.DrugstoreSecret);
            Assert.Equal(sample.TransactionStart, value.TransactionStart);
        }
    }
}
