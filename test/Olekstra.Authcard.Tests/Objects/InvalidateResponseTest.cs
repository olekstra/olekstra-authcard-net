﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;

    public class InvalidateResponseTest : SerializationTestsBase<InvalidateResponse>
    {
        private static readonly InvalidateResponse sample = new InvalidateResponse()
        {
            Success = true,
        };

        public InvalidateResponseTest()
            : base(sample, null, null)
        {
            // Nothing
        }

        public override void ValidateObject(InvalidateResponse value)
        {
            Assert.Equal(sample.Success, value.Success);
        }
    }
}
