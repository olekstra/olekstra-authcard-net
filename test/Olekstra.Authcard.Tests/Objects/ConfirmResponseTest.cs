﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;
    using System;

    public class ConfirmResponseTest : SerializationTestsBase<ConfirmResponse>
    {
        private static readonly ConfirmResponse sample = new ConfirmResponse()
        {
            AuthorizationCode = "1234567899",
            VoucherId = Guid.NewGuid().ToString()
        };

        public ConfirmResponseTest()
            : base(sample, null, null)
        {
            // Nothing
        }

        public override void ValidateObject(ConfirmResponse value)
        {
            Assert.Equal(sample.AuthorizationCode, value.AuthorizationCode);
            Assert.Equal(sample.VoucherId, value.VoucherId);
        }

        [Fact]
        public void TestSample4()
        {
            var value = authcardService.DeserializeJson<ConfirmResponse>(Samples.Sample4);

            Assert.NotNull(value);
            Assert.Equal("6dd07b70-a448-47e9-8fcd-07f8707f34cf", value.VoucherId);
            Assert.Equal("1567", value.AuthorizationCode);
        }
    }
}
