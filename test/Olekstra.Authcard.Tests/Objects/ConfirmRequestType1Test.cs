﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;
    using System;

    public class ConfirmRequestType1Test : SerializationTestsBase<ConfirmRequestType1>
    {
        private static readonly ConfirmRequestType1 sample = new ConfirmRequestType1()
        {
            ConfirmationToken = "6f3ee55f-66ca-4ba8-ae54-6ad0387a9f33",
            DrugstoreId = "testdrugstoreid",
            VoucherId = "7fa97103-c63e-4492-be2b-becfb9e679e9",
        };

        private static readonly string sampleJson = @"
{
""cOnFiRmAtIoNtOkEn"":""6f3ee55f-66ca-4ba8-ae54-6ad0387a9f33"",
""drUgsTorEid"":""testdrugstoreid"",
""voUchEriD"":""7fa97103-c63e-4492-be2b-becfb9e679e9"",
}";

        public ConfirmRequestType1Test()
            : base(sample, null, sampleJson)
        {
            // Nothing
        }

        public override void ValidateObject(ConfirmRequestType1 value)
        {
            Assert.Equal(sample.ConfirmationToken, value.ConfirmationToken);
            Assert.Equal(sample.DrugstoreId, value.DrugstoreId);
            Assert.Equal(sample.VoucherId, value.VoucherId);
        }

        [Fact]
        public void TestSample3()
        {
            var value = authcardService.DeserializeJson<ConfirmRequestType1>(Samples.Sample3);

            Assert.NotNull(value);
            Assert.Equal("aea1e8bc-3101-47d8-973e-63011d991dd5", value.ConfirmationToken);
            Assert.Equal("PHARMANET", value.DrugstoreId);
            Assert.Equal("6dd07b70-a448-47e9-8fcd-07f8707f34cf", value.VoucherId);
        }
    }
}
