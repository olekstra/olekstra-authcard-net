﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;
    using System;

    public class InvalidateRequestTest : SerializationTestsBase<InvalidateRequest>
    {
        private static readonly InvalidateRequest sample = new InvalidateRequest()
        {
            DrugstoreId = "testdrugstoreid",
            VoucherId = Guid.NewGuid().ToString(),
            AuthorizationCode = "123456789",
            Reason = "test",
            VoucherDate = DateTime.Now.Date,
        };

        public InvalidateRequestTest()
            : base(sample, null, null)
        {
            // Nothing
        }

        public override void ValidateObject(InvalidateRequest value)
        {
            Assert.Equal(sample.AuthorizationCode, value.AuthorizationCode);
            Assert.Equal(sample.DrugstoreId, value.DrugstoreId);
            Assert.Equal(sample.VoucherId, value.VoucherId);
            Assert.Equal(sample.Reason, value.Reason);
            Assert.Equal(sample.VoucherDate.Date, value.VoucherDate);
        }
    }
}
