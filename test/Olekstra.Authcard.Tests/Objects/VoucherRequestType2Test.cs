﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;
    using System.Collections.Generic;
    using System;

    public class VoucherRequestType2Test : SerializationTestsBase<VoucherRequestType2>
    {
        private static readonly VoucherRequestType2 sample = new VoucherRequestType2()
        {
            CardBarcode = "1234567899",
            DrugstoreId = "testdrugstoreid",
            Product = new List<VoucherRequestType1Product>
            {
                new VoucherRequestType1Product() { Barcode = "12345678910", ClientPackId = "123456789000", ClientPackName = "Test Pack Name",  DecNumber = "1234567890", Ekt = "10101010", Price = 100, Quantity = "1" },
                new VoucherRequestType1Product() { Barcode = "01234567890", ClientPackId = "012345678900", ClientPackName = "Test Pack Name 2",  DecNumber = "0123456789", Ekt = "20202020", Price = 200, Quantity = "2" },

            },
            SumToPay = (decimal)300.5,
            VoucherId = Guid.NewGuid().ToString(),
            TransactionStart = DateTime.Now,
            DrugstoreSecret = "testdrucstoresicret",
            DisableDiscounts = true,
            DisableNotifications = false,
            ExactNonRefundSum = 10,
            ExactRefundSum = (decimal)5.3,
            ExactSumToPay = (decimal)4.7
        };

        public VoucherRequestType2Test()
            : base(sample, null, null)
        {
            // Nothing
        }

        public override void ValidateObject(VoucherRequestType2 value)
        {
            Assert.Equal(sample.CardBarcode, value.CardBarcode);
            Assert.Equal(sample.DrugstoreId, value.DrugstoreId);
            Assert.Equal(sample.SumToPay, value.SumToPay);
            Assert.Equal(sample.VoucherId, value.VoucherId);

            Assert.Equal(sample.TransactionStart, value.TransactionStart);
            Assert.Equal(sample.DrugstoreSecret, value.DrugstoreSecret);
            Assert.Equal(sample.DisableDiscounts, value.DisableDiscounts);
            Assert.Equal(sample.DisableNotifications, value.DisableNotifications);
            Assert.Equal(sample.ExactNonRefundSum, value.ExactNonRefundSum);
            Assert.Equal(sample.ExactRefundSum, value.ExactRefundSum);
            Assert.Equal(sample.ExactSumToPay, value.ExactSumToPay);

            Assert.Equal(sample.Product[0].Barcode, value.Product[0].Barcode);
            Assert.Equal(sample.Product[0].ClientPackId, value.Product[0].ClientPackId);
            Assert.Equal(sample.Product[0].ClientPackName, value.Product[0].ClientPackName);
            Assert.Equal(sample.Product[0].DecNumber, value.Product[0].DecNumber);
            Assert.Equal(sample.Product[0].Ekt, value.Product[0].Ekt);
            Assert.Equal(sample.Product[0].Price, value.Product[0].Price);
            Assert.Equal(sample.Product[0].Quantity, value.Product[0].Quantity);
            Assert.Equal(sample.Product[1].Barcode, value.Product[1].Barcode);
            Assert.Equal(sample.Product[1].ClientPackId, value.Product[1].ClientPackId);
            Assert.Equal(sample.Product[1].ClientPackName, value.Product[1].ClientPackName);
            Assert.Equal(sample.Product[1].DecNumber, value.Product[1].DecNumber);
            Assert.Equal(sample.Product[1].Ekt, value.Product[1].Ekt);
            Assert.Equal(sample.Product[1].Price, value.Product[1].Price);
            Assert.Equal(sample.Product[1].Quantity, value.Product[1].Quantity);
        }
    }
}
