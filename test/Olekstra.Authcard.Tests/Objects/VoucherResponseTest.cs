﻿namespace Olekstra.Authcard.Tests.Objects
{
    using Xunit;
    using Olekstra.Authcard.Objects;
    using System.Collections.Generic;
    using System;

    public class VoucherResponseTest : SerializationTestsBase<VoucherResponse>
    {
        private static readonly VoucherResponse sample = new VoucherResponse()
        {
            DrugstoreId = "testdrugstoreid",
            SumToPay = (decimal)300.5,
            VoucherId = Guid.NewGuid().ToString(),
            AuthorizationCode = "123456789",
            Card = new VoucherResponseCard()
            {
                Active = true,
                Check = new List<string> { "test check1", "test check2" },
                Comment = "test comment",
                Number = "123456789",
                Owner = "Some Owner",
                PlanId = "TEST_PLAN",
                PlanName = "Test Plan"
            },
            Complete = false,
            ConfirmationToken = Guid.NewGuid().ToString(),
            DiscountSum = 10,
            ExactNonRefundSum = (decimal)13.3,
            ExactRefundSum = (decimal)2.2,
            ExactSumToPay = 1100,
            SumWithoutDiscount = 23043,
            Warning = new List<string> { "test warn", "test warn2" },
            Row = new List<VoucherResponseRow>
            {
                new VoucherResponseRow()
                {
                    Id = "123412",
                    DiscountPercent = 50,
                    Name = "Row Name",
                    ClientPackId = "123456789000",
                    ClientPackName = "Test Pack Name",
                    SumToPay = 100,
                    Price = 100,
                    Quantity = "1",
                    DiscountSum = 10,
                    ExactNonRefundSum = (decimal)13.3,
                    ExactRefundSum = (decimal)2.2,
                    ExactSumToPay = 1100,
                    SumWithoutDiscount = 23043,
                    Warning = new List<string> { "test warn", "test warn2" },
                    RefundSum = (decimal) 99.9,
                    ReplacePack = new List<VoucherResponseRowReplacePack>()
                    {
                         new VoucherResponseRowReplacePack()
                         {
                             Barcode = "12345678987",
                             PackId = "1234674456456",
                             TradeName = "test replace name"
                         },
                         new VoucherResponseRowReplacePack()
                         {
                              Barcode = "4617856548",
                              PackId = "146744867856456",
                              TradeName = "test replace name 2"
                         },
                    },
                },
                new VoucherResponseRow()
                {
                    Id = "123412",
                    DiscountPercent = 50,
                    Name = "Row Name",
                    ClientPackId = "123456789000",
                    ClientPackName = "Test Pack Name",
                    SumToPay = 100,
                    Price = 100,
                    Quantity = "1",
                    DiscountSum = 10,
                    ExactNonRefundSum = (decimal)13.3,
                    ExactRefundSum = (decimal)2.2,
                    ExactSumToPay = 1100,
                    SumWithoutDiscount = 23043,
                    Warning = new List<string> { "test warn", "test warn2" },
                    RefundSum = (decimal) 99.9
                },
            },
        };

        public VoucherResponseTest()
            : base(sample, null, null)
        {
            // Nothing
        }

        public override void ValidateObject(VoucherResponse value)
        {
            Assert.Equal(sample.AuthorizationCode, value.AuthorizationCode);
            Assert.Equal(sample.DrugstoreId, value.DrugstoreId);
            Assert.Equal(sample.SumToPay, value.SumToPay);
            Assert.Equal(sample.VoucherId, value.VoucherId);
            Assert.Equal(sample.Complete, value.Complete);
            Assert.Equal(sample.ConfirmationToken, value.ConfirmationToken);
            Assert.Equal(sample.DiscountSum, value.DiscountSum);
            Assert.Equal(sample.ExactNonRefundSum, value.ExactNonRefundSum);
            Assert.Equal(sample.SumWithoutDiscount, value.SumWithoutDiscount);
            Assert.Equal(sample.ExactSumToPay, value.ExactSumToPay);
            Assert.Equal(sample.ExactRefundSum, value.ExactRefundSum);
            Assert.Equal(sample.Warning[0], value.Warning[0]);
            Assert.Equal(sample.Warning[1], value.Warning[1]);

            Assert.Equal(sample.Card.Active, value.Card.Active);
            Assert.Equal(sample.Card.Check, value.Card.Check);
            Assert.Equal(sample.Card.Comment, value.Card.Comment);
            Assert.Equal(sample.Card.Number, value.Card.Number);
            Assert.Equal(sample.Card.Owner, value.Card.Owner);
            Assert.Equal(sample.Card.PlanId, value.Card.PlanId);
            Assert.Equal(sample.Card.PlanName, value.Card.PlanName);

            Assert.Equal(sample.Row[0].DiscountPercent, value.Row[0].DiscountPercent);
            Assert.Equal(sample.Row[0].ClientPackId, value.Row[0].ClientPackId);
            Assert.Equal(sample.Row[0].ClientPackName, value.Row[0].ClientPackName);
            Assert.Equal(sample.Row[0].DiscountSum, value.Row[0].DiscountSum);
            Assert.Equal(sample.Row[0].ExactNonRefundSum, value.Row[0].ExactNonRefundSum);
            Assert.Equal(sample.Row[0].ExactRefundSum, value.Row[0].ExactRefundSum);
            Assert.Equal(sample.Row[0].Quantity, value.Row[0].Quantity);
            Assert.Equal(sample.Row[0].ExactSumToPay, value.Row[0].ExactSumToPay);
            Assert.Equal(sample.Row[0].Id, value.Row[0].Id);
            Assert.Equal(sample.Row[0].Name, value.Row[0].Name);
            Assert.Equal(sample.Row[0].Price, value.Row[0].Price);
            Assert.Equal(sample.Row[0].RefundSum, value.Row[0].RefundSum);
            Assert.Equal(sample.Row[0].SumToPay, value.Row[0].SumToPay);
            Assert.Equal(sample.Row[0].SumWithoutDiscount, value.Row[0].SumWithoutDiscount);
            Assert.Equal(sample.Row[0].Warning[0], value.Row[0].Warning[0]);
            Assert.Equal(sample.Row[0].Warning[1], value.Row[0].Warning[1]);
            Assert.Equal(sample.Row[0].ReplacePack[0].Barcode, value.Row[0].ReplacePack[0].Barcode);
            Assert.Equal(sample.Row[0].ReplacePack[0].PackId, value.Row[0].ReplacePack[0].PackId);
            Assert.Equal(sample.Row[0].ReplacePack[0].TradeName, value.Row[0].ReplacePack[0].TradeName);
            Assert.Equal(sample.Row[0].ReplacePack[1].Barcode, value.Row[0].ReplacePack[1].Barcode);
            Assert.Equal(sample.Row[0].ReplacePack[1].PackId, value.Row[0].ReplacePack[1].PackId);
            Assert.Equal(sample.Row[0].ReplacePack[1].TradeName, value.Row[0].ReplacePack[1].TradeName);

            Assert.Equal(sample.Row[1].DiscountPercent, value.Row[1].DiscountPercent);
            Assert.Equal(sample.Row[1].ClientPackId, value.Row[1].ClientPackId);
            Assert.Equal(sample.Row[1].ClientPackName, value.Row[1].ClientPackName);
            Assert.Equal(sample.Row[1].DiscountSum, value.Row[1].DiscountSum);
            Assert.Equal(sample.Row[1].ExactNonRefundSum, value.Row[1].ExactNonRefundSum);
            Assert.Equal(sample.Row[1].ExactRefundSum, value.Row[1].ExactRefundSum);
            Assert.Equal(sample.Row[1].Quantity, value.Row[1].Quantity);
            Assert.Equal(sample.Row[1].ExactSumToPay, value.Row[1].ExactSumToPay);
            Assert.Equal(sample.Row[1].Id, value.Row[1].Id);
            Assert.Equal(sample.Row[1].Name, value.Row[1].Name);
            Assert.Equal(sample.Row[1].Price, value.Row[1].Price);
            Assert.Equal(sample.Row[1].RefundSum, value.Row[1].RefundSum);
            Assert.Equal(sample.Row[1].SumToPay, value.Row[1].SumToPay);
            Assert.Equal(sample.Row[1].SumWithoutDiscount, value.Row[1].SumWithoutDiscount);
            Assert.Equal(sample.Row[1].Warning[0], value.Row[1].Warning[0]);
            Assert.Equal(sample.Row[1].Warning[1], value.Row[1].Warning[1]);
            Assert.Equal(sample.Row[1].ReplacePack, value.Row[1].ReplacePack);
        }

        [Fact]
        public void SampleText2()
        {
            var value = authcardService.DeserializeJson<VoucherResponse>(Samples.Sample2);

            Assert.NotNull(value);
            Assert.Equal("d555bcda-7c8e-4238-815e-cb805804a660", value.VoucherId);
            Assert.Equal("123_TEST_456ZZZ_789", value.DrugstoreId);
            Assert.Equal("3f619d02-db1d-4bc4-8376-3713558353bd", value.ConfirmationToken);
            Assert.Equal("6830010100000", value.Card.Number);
            Assert.Equal("Тест-карта проверки аптек №72", value.Card.Owner);
            Assert.True(value.Card.Active);
            Assert.Equal("MUSLI_YOGURT_1_CARD", value.Card.PlanId);
            Assert.Equal("Мюсли и Йогурт - карта 1.2", value.Card.PlanName);

            Assert.Equal(10000M, value.SumWithoutDiscount);
            Assert.Equal(10000.01M, value.SumToPay);
            Assert.Equal(1.01M, value.DiscountSum);

            Assert.Single(value.Row);

            Assert.Equal("Танакан таб 40мг №90", value.Row[0].Name);
            Assert.Equal("FR22198", value.Row[0].Id);
            Assert.Equal("200302892011", value.Row[0].ClientPackId);
            Assert.Equal("5", value.Row[0].Quantity);
            Assert.Equal(2000M, value.Row[0].Price);
            Assert.Equal(10001M, value.Row[0].SumToPay);
            Assert.Equal(111M, value.Row[0].DiscountSum);
            Assert.Equal(11, value.Row[0].DiscountPercent);
            Assert.Equal(0.11M, value.Row[0].RefundSum);
            Assert.Equal(10000.10M, value.Row[0].SumWithoutDiscount);
            Assert.Equal(5, value.Row[0].NonRefundPercent);

            Assert.Equal("Исчерпан лимит по группе \"Первая покупка (без СМС)\"", value.Row[0].Warning[0]);
            Assert.Equal("Исчерпан лимит по группе \"Вторая покупка (без СМС)\"", value.Row[0].Warning[1]);
            Assert.Equal("Исчерпан месячный лимит по группе \"Все товары\"", value.Row[0].Warning[2]);

            Assert.False(value.Complete);
        }
    }
}
