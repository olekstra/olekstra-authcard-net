﻿namespace BatchConfirmTool
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Olekstra.Authcard;
    using Olekstra.Authcard.Objects;

    public class Program
    {
        private readonly ILogger logger;

        private readonly IAuthcardService authcardService;

        public static async Task Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false)
                .AddEnvironmentVariables()
                .AddUserSecrets<Program>()
                .AddCommandLine(args)
                .Build();

            var services = new ServiceCollection()
                .AddLogging(o => o.AddConfiguration(config.GetSection("Logging")).AddConsole())
                .AddSingleton<IConfiguration>(config)
                .AddSingleton<Program>()
                .AddSingleton<IAuthcardService, AuthcardService>()
                .Configure<AuthcardServiceOptions>(config.GetSection("Authcard"))
                .BuildServiceProvider();

            var taskCancellationSource = new CancellationTokenSource();
            Console.TreatControlCAsInput = false;
            Console.CancelKeyPress += (s, e) => taskCancellationSource.Cancel();

            Console.WriteLine("STARTING. Press Ctrl+C to break.");

            var drugstoreSecret = config["DrugstoreSecret"];

            var program = services.GetRequiredService<Program>();
            await program.RunAsync(drugstoreSecret, taskCancellationSource.Token);

            Console.WriteLine("FINISH. DONE.");
        }

        public Program(IAuthcardService authcardService, ILogger<Program> logger)
        {
            this.authcardService = authcardService;
            this.logger = logger;
        }

        public async Task RunAsync(string drugstoreSecret, CancellationToken cancellationToken)
        {
            var input = await File.ReadAllLinesAsync("input.csv", cancellationToken);

            var output = new List<string>(input.Length);

            foreach (var line in input)
            {
                cancellationToken.ThrowIfCancellationRequested();

                var parts = line.Split('\t');

                if (parts[0] == "cheque_id")
                {
                    output.Add(line + '\t' + "authCode");
                    continue;
                }

                var drugstoreId = parts[2];
                var date = DateTimeOffset.Parse(parts[1]).DateTime;
                var voucherId = parts[4];
                var confirmationToken = parts[7];

                var req = new ConfirmRequestType2()
                {
                    DrugstoreId = drugstoreId,
                    TransactionStart = date,
                    VoucherId = voucherId,
                    ConfirmationToken = confirmationToken,
                    DrugstoreSecret = drugstoreSecret,
                };

                var resp = await authcardService.ConfirmExAsync(req);
                var authCode = resp.AuthorizationCode;

                output.Add(line + '\t' + authCode);

                logger.LogInformation(
                    "{0} {1} {2} - {3}",
                    voucherId,
                    date.ToShortDateString(),
                    drugstoreId,
                    authCode);
            }

            await File.WriteAllLinesAsync("output.csv", output, cancellationToken);
        }
    }
}
